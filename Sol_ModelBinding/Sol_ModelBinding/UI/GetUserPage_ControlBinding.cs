﻿using Sol_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_ModelBinding.UI
{
    public partial class GetUserPage
    {
        #region Property
        private String FirstNameBinding
        {
            get
            {
                return txtFirstName.Text;
            }
            set
            {
                txtFirstName.Text = value;
            }
        }

        private String LastNameBinding
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }

        private Boolean GenderBinding
        {
            get
            {
                return
                    (rblGender
                    .Items
                    .Cast<ListItem>()
                    .AsEnumerable()
                    .FirstOrDefault((leListItemObj) => leListItemObj.Selected == true)
                    .Text) == "Male" ? true : false;

            }
            set
            {
                bool flag = value;

                rblGender
                    .Items
                    .Cast<ListItem>()
                    .AsEnumerable()
                    .ToList()
                    .ForEach((leListItemObj) =>
                    {
                        if (flag == true)
                        {
                            if (leListItemObj.Text == "Male")
                            {
                                leListItemObj.Selected = true;
                            }
                            else
                            {
                                leListItemObj.Selected = false;
                            }
                        }
                        else
                        {
                            if (leListItemObj.Text == "Female")
                            {
                                leListItemObj.Selected = true;
                            }
                            else
                            {
                                leListItemObj.Selected = false;
                            }
                        }


                    });
            }

        }

        private List<HobbyEntity> HobbyBinding
        {
            get
            {

                return chkHobby
                    .Items
                    .Cast<ListItem>()
                    .AsEnumerable()
                    .Select((leCheckBoxListObj) => new HobbyEntity()
                    {
                        HobbyName = leCheckBoxListObj.Text,
                        Interested = leCheckBoxListObj.Selected
                    })
                   .ToList();

            }
            set
            {
                //chkHobby.DataSource = value;
                //chkHobby.DataBind();

                // Selected Property Bind for selected Interested Checkbox from Entity to ui
                foreach (ListItem lstObj in chkHobby.Items)
                {
                    lstObj.Selected = Convert.ToBoolean(
                            value
                            ?.AsEnumerable()
                            ?.Where((leUserentityObj) => leUserentityObj.HobbyName == lstObj.Text)
                            ?.FirstOrDefault()
                            ?.Interested);
                }
            }
        }
        #endregion

    }
}