﻿using Sol_ModelBinding.DAL;
using Sol_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ModelBinding.UI
{
    public partial class GetUserPage
    {
        #region Declaration
        private UserDal userDalObj = null;
        #endregion

        public async Task UserMappingEntityToUiData(UserEntity userEntityObj)
        {
            await Task.Run(() => {

                // Mapping
                this.FirstNameBinding = userEntityObj.FirstName;
                this.LastNameBinding = userEntityObj.LastName;
                this.GenderBinding = Convert.ToBoolean(userEntityObj.Gender);
                this.HobbyBinding = userEntityObj.Hobby;
            });
        }

        public async Task BindUserData()
        {
            // Create an Insatnce of User Dal
            userDalObj = new UserDal();

            // get User Data from Dal 
            var getUserData = await userDalObj.GetUserData(new UserEntity()
            {
                UserId = 1
            });

            //Binding
            await this.UserMappingEntityToUiData(getUserData);
        }


    }
}