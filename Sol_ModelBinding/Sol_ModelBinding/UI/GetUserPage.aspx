﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="GetUserPage.aspx.cs" Inherits="Sol_ModelBinding.UI.GetUserPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        body {
            padding: 10px;
            margin: auto;
        }

        .divCenterPosition {
            margin: 0px auto;
            margin-top: 130px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <div class="divCenterPosition" style="width: 20%">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" PlaceHolder="First Name"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" PlaceHolder="Last Name"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <asp:RadioButtonList ID="rblGender" runat="server" SelectMethod="BindUserData">
                                        <asp:ListItem Selected="False" Text="Male" Value="1"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Female" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="chkHobby" runat="server" DataTextField="HobbyName" DataValueField="HobbyId" RepeatColumns="1" RepeatLayout="Table" SelectMethod="BindUserData">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Submit" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
