﻿using Sol_ModelBinding.DAL.ORD;
using Sol_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_ModelBinding.DAL
{
    public class UserDal
    {
        #region Declaration
        private UserDcDataContext dc = null;
        #endregion

        #region constructor
        public UserDal()
        {
            dc = new UserDcDataContext();
        }
        #endregion

        #region PublicMethod
        public async Task<UserEntity> GetUserData(UserEntity userEntityObj)
        {
            return await Task.Run(() => {

                return

                  dc
                  ?.uspGetUsers(
                      "UserData",
                      userEntityObj.UserId
                      )
                  ?.AsEnumerable()
                  ?.Select(
                        (leUspGetUsersResultSetObj) => new UserEntity()
                        {
                            UserId = leUspGetUsersResultSetObj.UserId,
                            FirstName = leUspGetUsersResultSetObj.FirstName,
                            LastName = leUspGetUsersResultSetObj.LastName,
                            Gender=leUspGetUsersResultSetObj.Gender,
                            Hobby = (this.GetUserHobbyData(new HobbyEntity()
                            {
                                UserId = leUspGetUsersResultSetObj.UserId
                            })).Result as List<HobbyEntity>
                        })

                    ?.ToList()
                    ?.FirstOrDefault();
            });
        }

        #endregion

        #region Private Method
        private async Task<IEnumerable<HobbyEntity>> GetUserHobbyData(HobbyEntity userHobbyEntityObj)
        {
            return await Task.Run(() => {

                return
                    dc
                    ?.uspGetUsers(
                        "UserHobbyData",
                        userHobbyEntityObj.UserId
                        )
                    ?.AsEnumerable()
                    ?.Select((leUspGetUsersResultSetObj) => new HobbyEntity()
                    {
                        HobbyId = leUspGetUsersResultSetObj.HobbyId,
                        HobbyName = leUspGetUsersResultSetObj.HobbyName,
                        Interested = leUspGetUsersResultSetObj.Interested
                    }).ToList();
            });
        }
        #endregion
    }
}