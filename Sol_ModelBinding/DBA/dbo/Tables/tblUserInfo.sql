﻿CREATE TABLE [dbo].[tblUserInfo] (
    [UserId]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50)   NULL,
    [LastName]  VARCHAR (50)   NULL,
    [Gender]    BIT            NULL,
    [CityId]    NUMERIC (18)   NULL,
    [BirthDate] DATETIME       NULL,
    [Age]       INT            NULL,
    [ImagePath] NVARCHAR (MAX) NULL,
    [MobileNo]  VARCHAR (50)   NULL,
    CONSTRAINT [PK_tblUserInfo] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

