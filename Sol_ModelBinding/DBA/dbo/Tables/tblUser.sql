﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [Gender]    BIT          NULL,
    CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

