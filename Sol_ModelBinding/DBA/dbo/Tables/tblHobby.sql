﻿CREATE TABLE [dbo].[tblHobby] (
    [HobbyId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [HobbyName]  VARCHAR (50) NULL,
    [Interested] BIT          NULL,
    [UserId]     VARCHAR (50) NULL,
    CONSTRAINT [PK_tblHobby] PRIMARY KEY CLUSTERED ([HobbyId] ASC)
);

