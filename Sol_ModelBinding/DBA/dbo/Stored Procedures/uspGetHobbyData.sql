﻿CREATE PROCEDURE [dbo].[uspGetHobbyData]
	@Command Varchar(100)=NULL,

	@HobbyId numeric(18,0)=NULL,
	@HobbyName varchar(50)=NULL,
	@Interested bit=NULL,
	@UserId Numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
AS
BEGIN

	DECLARE @ErrorMessage Varchar(MAX)

	IF @Command='SelectAllUserHobbyData'
			BEGIN
				BEGIN TRANSACTION

				BEGIN TRY
					SELECT 
						UH.HobbyId,
						UH.HobbyName,
						UH.Interested,
						UH.UserId
						FROM tblUserInfoHobby AS UH
						

					SET @Status=1
					SET @Message='Get all UserHobby Data'

						COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
						
					SET @Status=0
					SET @Message='Select Exception'

					RAISERROR(@ErrorMessage,16,1)
				END CATCH

			END
			END