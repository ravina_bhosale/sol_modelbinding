﻿CREATE PROCEDURE uspSearchUser
    @FirstName  VARCHAR(50)=NULL,
    @LastName   VARCHAR(50)=NULL,
    @MobileNo VARCHAR(10)=NULL
AS
BEGIN

    DECLARE @Query NVARCHAR(MAX) = N'SELECT * FROM tblUserInfo WHERE '
    DECLARE  @ParamDefinition NVARCHAR(MAX) = N'@FirstName VARCHAR(20),
                                                @LastName VARCHAR(20),
                                                @MobileNo VARCHAR(10)'

   IF @FirstName IS NOT NULL
		BEGIN
			SELECT @Query=@Query + 'FirstName='+''''+@FirstName+''''
		END
	ELSE IF @LastName IS NOT NULL
		BEGIN
			SELECT @Query=@Query + ' LastName='+''''+@LastName+''''
		END

   ELSE IF @MobileNo IS NOT NULL
		BEGIN
			SELECT @Query=@Query + 'MobileNo='+''''+ @MobileNo+''''
		END
    EXECUTE sp_executesql @Query, @ParamDefinition, @FirstName, @LastName, @MobileNo;

END