﻿CREATE PROCEDURE uspSetUserInfo
(	@Command Varchar(MAX)=NULL,
	
	@UserId numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@Gender bit=NULL,
	@CityId numeric(18,0)=NULL,
	@BirthDate Datetime= NULL,
	@Age int = NULL,
	@MobileNo varchar(10)=NULL,
	@ImagePath nvarchar(max)=Null,

	@Status int=NULL OUT,
	@Message Varchar(MAX)= NULL OUT,
	@Id Numeric(18,0)=NULL OUT
)
AS
	
	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Add'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
					INSERT INTO tblUserInfo
					(
						
						FirstName,
						LastName,
						Gender,
						CityId,
						BirthDate,
						Age,
						MobileNo,
						ImagePath
					)
					VALUES
					(
						@FirstName,
						@LastName,
						@Gender,
						@CityId,
						@BirthDate,
						@Age,
						@MobileNo,
						@ImagePath
					)

					SELECT @Id =@@IDENTITY

					SET @Status=1
					SET @Message='Insert Success'

					 

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END 

			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

						SELECT 
						@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
						@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END,
						@Gender=CASE WHEN @Gender IS NULL THEN U.Gender ELSE @Gender END,
						@CityId=CASE WHEN @CityId IS NULL THEN U.CityId ELSE @CityId END,
						@BirthDate=CASE WHEN @BirthDate IS NULL THEN U.BirthDate ELSE @BirthDate END,
						@Age=CASE WHEN @Age IS NULL THEN U.Age ELSE @Age END,
						@MobileNo=CASE WHEN @MobileNo IS NULL THEN U.MobileNo ELSE @MobileNo END,
						@ImagePath=CASE WHEN @ImagePath IS NULL THEN U.ImagePath ELSE @ImagePath END
						FROM tblUserInfo AS U
							WHERE U.UserId=@UserId

							UPDATE tblUserInfo 
								SET 
									FirstName=@FirstName,
									LastName=@LastName,
									Gender=@Gender,
									CityId=@CityId,
									BirthDate=@BirthDate,
									Age=@Age,
									MobileNo=@MobileNo,
									ImagePath=@ImagePath
										WHERE UserId=@UserId
										
								SET @Status=1
								SET @Message='Update Succesfully'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


			END

	END