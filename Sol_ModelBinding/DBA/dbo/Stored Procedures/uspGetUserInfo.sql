﻿CREATE PROCEDURE [dbo].[uspGetUserInfo]
	@Command Varchar(100)=NULL,

	@UserId Numeric(18,0)=NULL,
	@FirstName varchar(50)=NULL,
	@LastName varchar(50)=NULL,
	@Gender bit=NULL,
	@BirthDate Date=NULL,
	@Age int =NULL,
	@CityId numeric(18,0)=NULL,
	@ImagePath nvarchar(max)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
AS
	BEGIN
		DECLARE @ErrorMessage Varchar(MAX)
		
		 IF @Command='SelectAllUserData'
			BEGIN
				BEGIN TRANSACTION

				BEGIN TRY
					SELECT 
						U.UserId,
						U.FirstName,
						U.LastName,
						U.Gender,
						U.BirthDate,
						U.Age,
						U.CityId,
						u.ImagePath
							FROM tblUserInfo AS U

					SET @Status=1
					SET @Message='Get all User Data'

						COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
						
					SET @Status=0
					SET @Message='Select Exception'

					RAISERROR(@ErrorMessage,16,1)
				END CATCH
			END

	END